﻿using System;
using System.Collections.Generic;

namespace ConsoleApp1
{
    public class GenericCycleList<T>:IAlgorithm<T> where T : IComparable
    {
     public class Node
        {
            public Node(T t) 
            { 
                next=null;
                data=t;
            }

            private Node next;
            public Node Next 
            {
                get { return next; }
                set { next = value; }
            }

            private T data;
            public T Data 
            {
                get { return data; }
                set { data = value; }
            }
        }
        public struct TSortStackItem
        {
            public int Level;
            public Node Item;
        }
        
        private Node head;

        private Node tail;
        public void AddHead(T t) //add node
        {
            Node n = new Node(t);
            n.Next = head;
            head = n;
            if (tail == null) tail = n;
                
        }
        public int cntNodes() //count nubmer of nodes
        {
            int cntNodes = 0;
            if (head!=null) cntNodes = 1;
            Node p = head;
            while (p != tail)
            {
                cntNodes++;
                p = p.Next;
            }
            return cntNodes;
        }
        public void DoCycleList() //connect ring
        {
            tail = null;
            Node p = head;
            while (p != null)
            {
                tail = p;
                p = p.Next;
            }
            tail.Next = head;
        }
        public void BreakCycleList() //disconnect ring
        {
            tail.Next = null;
            tail = null;
        }
        public void CycleList() 
        {
            tail.Next = head;
        }
        public IEnumerator<T> GetEnumerator() 
        {
            
            Node current = head;
            yield return current.Data;
            do
            {
                current = current.Next;
                yield return current.Data;
            }
            while (current.Next != head);
        }
        public bool IsSorted()
        {
            Node cur;
            Node p = head;
            while (p != null)
            {
                cur = p;
                p = p.Next;
                if (cur == tail) break;
                if (cur.Data.CompareTo(p.Data) == 1) return false;
            }
            return true;
        }
        public int BinarySearch(Node value)
        {
            int cntNodes = this.cntNodes();
            Random random = new Random();
            if (!this.IsSorted())
                return random.Next(0, cntNodes) * (-1) - 1;
            if (head.Data.CompareTo(value.Data) > 1)
                return 0;
            if (value.Data.CompareTo(tail.Data) > 1)
                return (~cntNodes);
            Node p = head;
            int cnt = 0;
            while (p != null)
            {
                if (p.Data.CompareTo(value.Data) == 0) return cnt;
                if (p.Next.Data.CompareTo(value.Data) == 1) return ~(cnt + 1);
                cnt++;
                if (p.Data.CompareTo(tail.Data) == 0) break;
                p = p.Next;
            }
            return (~cntNodes);
        }
        public void Sort()
        {
            Node IntersectSorted(Node pList1, Node pList2)
            {
                Node pCurItem, listResult;
                Node p1, p2;
                p1 = pList1;
                p2 = pList2;
                if ((p1.Data.CompareTo(p2.Data) == -1) || (p1.Data.CompareTo(p2.Data) == 0))
                {
                    pCurItem = p1;
                    p1 = p1.Next;
                }
                else
                {
                    pCurItem = p2;
                    p2 = p2.Next;
                }
                listResult = pCurItem;
                while ((p1 != null) && (p2 != null))
                {
                    if ((p1.Data.CompareTo(p2.Data) == -1) || (p1.Data.CompareTo(p2.Data) == 0))//if (p1.Data <= p2.Data)
                    {
                        pCurItem.Next = p1;
                        pCurItem = p1;
                        p1 = p1.Next;
                    }
                    else
                    {
                        pCurItem.Next = p2;
                        pCurItem = p2;
                        p2 = p2.Next;
                    }
                }
                if (p1 != null)
                    pCurItem.Next = p1;
                else
                    pCurItem.Next = p2;
                return listResult;
            };

            BreakCycleList();
            TSortStackItem[] Stack = new TSortStackItem[31];
            int StackPos;
            Node p;
            StackPos = 0;
            p = this.head;
            while (p != null)
            {
                Stack[StackPos].Level = 1;
                Stack[StackPos].Item = p;
                p = p.Next;
                Stack[StackPos].Item.Next = null;
                ++StackPos;
                while ((StackPos > 1) && (Stack[StackPos - 1].Level == Stack[StackPos - 2].Level))
                {
                    Stack[StackPos - 2].Item = IntersectSorted(Stack[StackPos - 2].Item, Stack[StackPos - 1].Item);
                    ++(Stack[StackPos - 2].Level);
                    --StackPos;
                }
            }
            while (StackPos > 1)
            {
                Stack[StackPos - 2].Item = IntersectSorted(Stack[StackPos - 2].Item, Stack[StackPos - 1].Item);
                ++Stack[StackPos - 2].Level;
                --StackPos;
            }
            if (StackPos > 0)
                head = Stack[0].Item;

            DoCycleList();
        }
    }
}
