﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    
    interface IAlgorithm<T> where T:IComparable 
    {
        void Sort();
        int BinarySearch(GenericCycleList<T>.Node value);
    }
}
