﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This is a cycleList");

            GenericCycleList<int> list = new GenericCycleList<int>();

            Random random = new Random();

            for (int x = 0; x < 10; x++)
            {
                list.AddHead(random.Next(0, 9));
            }

            list.DoCycleList();

            foreach (int i in list)
            {
               Console.Write(i);
            }

            Console.WriteLine();

            if (list.IsSorted())
                Console.WriteLine("The CycleList is sorted");
            else
                Console.WriteLine("The CycleList is not sorted");

            list.Sort();

            Console.WriteLine("Now the CycleList is sorted");

            foreach (int i in list)
            {
                Console.Write(i);
            }

            Console.WriteLine();
            Console.WriteLine($"2 is on the place {list.BinarySearch(new GenericCycleList<int>.Node(2))}");
            Console.WriteLine($"5 is on the place {list.BinarySearch(new GenericCycleList<int>.Node(5))}");
            Console.WriteLine($"8 is on the place {list.BinarySearch(new GenericCycleList<int>.Node(8))}");
            Console.WriteLine("\nDone");
        }
    }
}
